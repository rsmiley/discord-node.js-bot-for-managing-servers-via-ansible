# What is it
Simple Node.js script for a Discord bot using Discord.js to automatically manage servers from within Discord Servers.

This simply requires the following module.
`npm install discord.js`

You'll also need an OAuth token to utilize with Discord. You can obtain one from (here)[https://discordapp.com/developers/applications/] after creating an application and visiting the following link *with your bot clientID in place*
https://discordapp.com/oauth2/authorize?&client_id=YOUR_BOT_CLIENT_ID&scope=bot&permissions=8

After you've got your token, place it in the last line of the script and then start it!
You will of course need to tweak it to match your needs and your commands / groups. 


# Important Note
This bot will run no commands if you're not in the "MyNode Agent" role. While I'm sure you'll change it, please take note of it.