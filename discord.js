/* MyNode Guardian Bot
 * Powered by Discord.js (npm) and Ansible (or whatever you'll use)
 * Written by Roger Smiley
 */

const Discord = require('discord.js');
const client = new Discord.Client();

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  if (msg.content === 'ping') {
    msg.reply('pong');
  }
});

client.on('message', msg => {
function sendCommand(cmd) {
  const { exec } = require('child_process');
      exec(cmd, (error, stdout, stderr) => {
        if (error) {
          if(!stderr.includes('ext.mynode')) { msg.reply(`exec error :: ${stderr.trim()}`) };
          return;
        }
	if(!stdout.includes('ext.mynode')) { msg.reply(`${stdout.trim()}`) };
      });
}

// Build the command activity
if(msg.content.startsWith('!')) {
  if(!msg.member.roles.some(role => role.name === 'Bot Admin')) { return msg.reply("You are not authorized to use these commands.") }
  // Engage the command center
  var message = msg.content.split(' ');
  var command = message['0'].replace('!', '');
  var option = message['2'];
  var arg = message['3'];
  var target = function() {
    switch(message['1']) {
      case 'build':
        return 'build.mynode.co';
      break;
      case 'x':
        return 'x.mynode.co'
      break;
      default:
        msg.reply("Invalid server selection");
        return 'ext';
      break;
    }
  }
if(target() == 'ext') { return }
}

if(command == 'uptime' || command == 'load') {
  sendCommand("sudo ansible " + target() + " -m shell -a uptime");
}
if(command == 'run' || command == 'cmd') {
  sendCommand("sudo ansible " + target() + " -m shell -a '" + command + "'");
}
if(command == 'apache') {
  switch(option) {
    case 'restart':
      sendCommand("sudo ansible " + target() + " -m service -a name=httpd,status=restarted");
    break;
    case 'start':
      sendCommand("sudo ansible " + target() + " -m service -a name=httpd,status=started");
    break;
    case 'stop':
      sendCommand("sudo ansible " + target() + " -m service -a name=httpd,status=stopped");
    break;
    case 'status':
      sendCommand("sudo ansible " + target() + " -m shell -a 'service httpd status | grep active'");
    break;
    default:
      msg.reply("Invalid option | Usage: !apache [host] [status|start|restart|stop]");
    break;
  }
}
if(command == 'nginx') {
  switch(option) {
    case 'restart':
      sendCommand("sudo ansible " + target() + " -m service -a name=nginx,status=restarted");
    break;
    case 'start':
      sendCommand("sudo ansible " + target() + " -m service -a name=nginx,status=started");
    break;
    case 'stop':
      sendCommand("sudo ansible " + target() + " -m service -a name=nginx,status=stopped");
    break;
    case 'status':
      sendCommand("sudo ansible " + target() + " -m shell -a 'service nginx status | grep active'");
    break;
    default:
      msg.reply("Invalid option | Usage: !nginx [host] [status|start|restart|stop]");
    break;
  }
}
if(command == 'mysql') {
  switch(option) {
    case 'restart':
      msg.reply("I'm afraid we cannot do that at this time.");
      //sendCommand("sudo ansible " + target() + " -m service -a name=mysql,status=restarted");
    break;
    case 'start':
      msg.reply("I'm afraid we cannot do that at this time.");
      //sendCommand("sudo ansible " + target() + " -m service -a name=mysql,status=started");
    break;
    case 'stop':
      msg.reply("I'm afraid we cannot do that at this time.");
      //sendCommand("sudo ansible " + target() + " -m service -a name=mysql,status=stopped");
    break;
    case 'status':
      sendCommand("sudo ansible " + target() + " -m shell -a 'service nginx mysql | grep active'");
    break;
    default:
      msg.reply("Invalid option | Usage: !mysql [host] [status|start|restart|stop]");
    break;
  }
}
});

client.login('YOUR-OAUTH-BOT-KEY');
